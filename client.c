#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <winsock2.h>
#include <windows.h>

int sigH = 0;

// Client Connections thread
DWORD WINAPI ThreadFunc(void* data) {
    char  server_reply[2000];
    int recv_size;
    SOCKET *s = (SOCKET *)data;

    printf("Client Thread Created\n");

    while(1){
        //Receive a reply from the server
        if((recv_size = recv(*s , server_reply , 2000 , 0)) == SOCKET_ERROR){
            printf("recv failed %d\n", recv_size);
            break;
        }else{
        //Add a NULL terminating character to make it a proper string before printing
            server_reply[recv_size] = '\0';
            puts(server_reply);
        }

        if(sigH){
            break;
        }
    }
    return 0;
}

// Ctrl-C Handler
BOOL WINAPI consoleHandler(DWORD signal) {

    if (signal == CTRL_C_EVENT){
        printf("Ctrl-C handled\n");
        sigH = 1;
    }

    return TRUE;
}

int main(int argc, char const *argv[]){
    // gcc .\client.c -o .\client.exe -lws2_32 to build
    // gcc .\client.c -o .\client.exe -L[directory] -l[dll name] to build with custom dlls
    // i686-w64-mingw32-g++ client.c -o client.exe ... 32bits
    clock_t start = clock();
    // Client Vars
    WSADATA wsa;
    SOCKET *s = (SOCKET *)malloc(sizeof(SOCKET));
    struct sockaddr_in server;
    char message[100];
     
    // CLIENT SIDE ------------------------------------------------
    // Initialize win_sock.dll in windows asquing for V2.2
    printf("Initialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0){
        printf("Failed. Error Code : %d",WSAGetLastError());
        return 1;
    }
    printf("Initialised.\n");

    // Initialize a socket with his params
    if((*s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET){
        printf("Could not create socket : %d" , WSAGetLastError());
    }
 
    printf("Socket created.\n");

    // Try to conect to google server using sockaddr_in struct
    server.sin_addr.s_addr = inet_addr("192.168.0.116");
    server.sin_family = AF_INET;
    server.sin_port = htons(8888);
 
    //Connect to remote server
    if (connect(*s , (struct sockaddr *)&server , sizeof(server)) < 0){
        puts("connect error");
        return 1;
    }

    if (!SetConsoleCtrlHandler(consoleHandler, TRUE)) {
        printf("\nERROR: Could not set control handler"); 
        return 1;
    }

    HANDLE thread = CreateThread(NULL, 0, ThreadFunc, s, 0, NULL);
    if (thread) {
        // Optionally do stuff, such as wait on the thread.

    }
    puts("Connected");
    puts("Start sending messages:");
    while(1){
        fgets (message, 100, stdin);
        
        if(sigH){
            break;
        }
        //Send some data
        if(send(*s , message , strlen(message) , 0) < 0){
            puts("Send failed");
            return 1;
        }
        printf("Data Sent - ");

        
    }

    // CleanUp and socket closing
    closesocket(*s);
    WSACleanup();
    free(s);
    s = NULL;

    // Time running function
    clock_t end = clock();
    printf("Elapsed: %f seconds\n", (double)(end - start)/CLOCKS_PER_SEC);
    return 0;
}