#include <stdio.h>
#include <winsock2.h>
#include <time.h>
#include <string.h>
#include <windows.h>

int sigH = 0;

// Struct for Server Messages Thread Handler
typedef struct ServerMessageThread{
    SOCKET **sockets;
    struct sockaddr_in *my_addr;
    int my_n;
}ServerMessageThread;

// Struct for Server Connections Thread Handler
typedef struct ServerConnectionThread{
    HANDLE **threads;
    int *total;
    SOCKET *socket;
}ServerConnectionThread;

// Ctrl-C Handler
BOOL WINAPI consoleHandler(DWORD signal) {

    if (signal == CTRL_C_EVENT){
        sigH = 1;
        printf("Ctrl-C handled\n");
    }

    return TRUE;
}

// Server Messages Thread Handler
DWORD WINAPI MessageThreadFunc(void* data) {
    ServerMessageThread *st = (ServerMessageThread *)data;
    int m, valread, MAXRECV = 1024;
    char *buffer, rcv_alert[50], n = 0;
    buffer = (char*) malloc((MAXRECV + 1) * sizeof(char));
    time_t t = time(NULL);
    struct tm *tm;

    printf("Server Thread #%d Created %s\n", (*st).my_n, inet_ntoa((*st->my_addr).sin_addr));
    while(1){
        valread = recv((*st->sockets[(*st).my_n]) , buffer, MAXRECV, 0);

        if(sigH){
            break;
        }
             
        if(valread == SOCKET_ERROR){
            int error_code = WSAGetLastError();
            if(error_code == WSAECONNRESET){
                //Somebody disconnected , get his details and print
                printf("Host disconnected unexpectedly , ip %s , port %d \n", inet_ntoa((*st->my_addr).sin_addr), 
                    ntohs((*st->my_addr).sin_port));
                break;
            }else{
                printf("recv failed with error code : %d\n", error_code);
                break;
            }
        }

        if(valread == 0){
            //Somebody disconnected , get his details and print
            printf("Host disconnected , ip %s , port %d \n", inet_ntoa((*st->my_addr).sin_addr), ntohs((*st->my_addr).sin_port));
            break;
        }else{
            //add null character, if you want to use with printf/puts or other string handling functions
            buffer[valread] = '\0';
            if(! strncmp(buffer, "\\exit", 5)){
                break;
            }else{
                tm = localtime(&t);
                printf("%d/%d at %d:%d:%d de ", (*tm).tm_mon + 1, (*tm).tm_mday, (*tm).tm_hour, (*tm).tm_min, (*tm).tm_sec);
                //strftime (buffer,80,"Now it's %I:%M%p.",timeinfo);
                m = sprintf(rcv_alert, "recibido el %d/%d at %d:%d:%d\0", (*tm).tm_mon + 1, (*tm).tm_mday, 
                    (*tm).tm_hour, (*tm).tm_min, (*tm).tm_sec);
                send((*st->sockets[(*st).my_n]), rcv_alert, m, 0);
                printf("%s:%d: %s", inet_ntoa((*st->my_addr).sin_addr), ntohs((*st->my_addr).sin_port), buffer);
            }
        }
    }

    // Cleanup
    free(tm);
    free(buffer);
    tm = NULL;
    buffer = NULL;
    printf("finished messages\n");
    return 0;
}

// Server Connections Thread Handler
DWORD WINAPI ConnectionThreadFunc(void* data) {
    ServerConnectionThread *targ = (ServerConnectionThread *)data;
    SOCKET *sockets[10];
    ServerMessageThread *st[10];
    struct sockaddr_in *clients[10];
    char *hello, rcv_alert[50];
    int c = sizeof(struct sockaddr_in);
    hello = "Connected to Server.\n";

    while((*targ->total) < 10){
    // Accept incoming connections
        if((*targ->total) < 10){
            clients[(*targ->total)] = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
            sockets[(*targ->total)] = (SOCKET *)malloc(sizeof(SOCKET));

            if((*sockets[(*targ->total)] = accept((*targ->socket) ,(struct sockaddr *)clients[(*targ->total)], &c)) != INVALID_SOCKET){
                puts("Connection accepted");
                send(*sockets[(*targ->total)], hello, strlen(hello) , 0);
                st[(*targ->total)] = (ServerMessageThread *)malloc(sizeof(ServerMessageThread));
                (*st[(*targ->total)]).sockets = sockets;
                (*st[(*targ->total)]).my_addr = clients[(*targ->total)];
                (*st[(*targ->total)]).my_n = (*targ->total);
                (*targ).threads[(*targ->total)] = CreateThread(NULL, 0, MessageThreadFunc, st[(*targ->total)], 0, NULL);
                ++(*targ->total);
            }
        }

        if(sigH){
            break;
        }
    }

    for (int i = 0; i < (*targ->total); ++i){
        if(*sockets[i] == INVALID_SOCKET){
            printf("accept failed with error code : %d", WSAGetLastError());
        }else{
            // Cleanup
            (*st[i]).my_addr = NULL;
            closesocket(*sockets[i]);
            free(sockets[i]);
            sockets[i] = NULL;
            free(clients[i]);
            clients[i] = NULL;
            (*st[i]).sockets = NULL;
            free(st[i]);
        }
    }
    hello = NULL;
    printf("finished connections\n");
    return 0;
}
 
int main(int argc , char *argv[]){
    // gcc .\server.c -o .\server.exe -lws2_32 to build
    WSADATA wsa;
    SOCKET *s = (SOCKET *)malloc(sizeof(SOCKET));
    struct sockaddr_in server;
    int n = 0;
    HANDLE threads[11];
    ServerConnectionThread *targ = (ServerConnectionThread *)malloc(sizeof(ServerConnectionThread));

    //Replies to the client
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2), &wsa) != 0){
        printf("Failed. Error Code : %d", WSAGetLastError());
        return 1;
    }
    printf("Initialised.\n");
     
    //Create a socket
    if((*s = socket(AF_INET, SOCK_STREAM, 0 )) == INVALID_SOCKET){
        printf("Could not create socket : %d", WSAGetLastError());
    }
    printf("Socket created.\n");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(8888);
     
    //Bind
    if(bind(*s ,(struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR){
        printf("Bind failed with error code : %d" , WSAGetLastError());
    }
    puts("Bind done");
    
    //Listen
    listen(*s , 3);

    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    (*targ).total = &n;
    (*targ->threads) = threads;
    (*targ).socket = s;


    if (!SetConsoleCtrlHandler(consoleHandler, TRUE)) {
        printf("\nERROR: Could not set control handler"); 
        return 1;
    }

    threads[10] = CreateThread(NULL, 0, ConnectionThreadFunc, targ, 0, NULL);

    while(1){
        if(sigH){
            break;
        }
    }

    for (int i = 0; i < n; ++i){
        if(threads[i]){
            WaitForSingleObject(threads[i], INFINITE);
        }
    }

    // Waiting for connection handlers threads and closing his socket
    closesocket(*s);
    WaitForSingleObject(threads[10], INFINITE);

    // Cleanup
    free(targ);
    WSACleanup();
    printf("finished main\n");
    return 0;
}