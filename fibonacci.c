#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <limits.h>

int fib(int a);
int fib_cache_topdown(int a, int *cache);
typedef struct HugeInteger{
    int *digits;
    int length;
}HugeInteger;
HugeInteger *hugeAdd(HugeInteger *a, HugeInteger *b);
HugeInteger *parseString(char *str);
HugeInteger *hugeDestroyer(HugeInteger *h);
char *fib_cache_botup(int a,HugeInteger *cache[]);

int main(int argc, char *argv[]){
    clock_t start = clock();
    int a = strtol(argv[1], NULL, 0);
    int cache2[a];
    HugeInteger *cache[3], *aux;
    memset(cache2, 0, sizeof(int)*a);
    for (int i = 0; i < 3; ++i){
        cache[i] = (HugeInteger *)malloc(sizeof(HugeInteger));
        (*cache[i]).length = 1;
        (*cache[i]).digits = (int *)malloc(sizeof(int));
        (*cache[i]).digits[0] = 1;
    }
    //printf("Fibonacci(%s): %d\n", argc, argv[1], argv[1], fib(a));
    //printf("Fibonacci(%s): %d\n", argv[1], fib_cache_topdown(a,cache2));
    printf("Fibonacci(%s): %s\n", argv[1], fib_cache_botup(a,cache));
    for (int i = 0; i < 2; ++i){
        cache[i] = hugeDestroyer(cache[i]);
    }
    cache[2] = NULL;
    clock_t end = clock();
    printf("Elapsed: %f seconds\n", (double)(end - start)/CLOCKS_PER_SEC);
    return 0;
}
/* Calculates fibonacci sequence */
int fib(int a){
    if(a < 0){ 
        return 0;
    }else if(a < 2 ){
        return a;
    }else{
        return fib(a - 1) + fib(a - 2);
    }
}

/* Calculates fibonacci sequence with cache */
int fib_cache_topdown(int a, int *cache){
    int result;
    if(a < 0){ 
        result = 0;
    }else if(a <= 2 ){
        result = 1;
    }else if(cache[a-1] != 0){
        return cache[a-1];
    }else{
        result = fib_cache_topdown(a - 1, cache) + fib_cache_topdown(a - 2, cache);
    }
    cache[a-1] = result;
    return result;
}

HugeInteger *hugeAdd(HugeInteger *a, HugeInteger *b){
    HugeInteger *h = (HugeInteger *)malloc(sizeof(HugeInteger)); 
    int len, aux, r = 0;
    if((*a).length >= (*b).length){
        len = (*a).length;
    }else{
        len = (*b).length;
    }

    int *number, *auxarr = (int *)malloc(sizeof(int)*(len));
    for (int i = 0; i < len; i++){
        if(i >= (*a).length){
            aux = (*b).digits[i] + r;
        }else if(i >= (*b).length){
            aux = (*a).digits[i] + r;
        // In out of bounds digits[i] == 0
        }else{
            aux = (*a).digits[i] + (*b).digits[i] + r;
        }
        auxarr[i] = aux % 10;
        r = aux/10;
    }

    if(r != 0){
        len++;
        number = (int *)malloc(sizeof(int)*(len));
        for (int i = 0; i < len-1; ++i){
            number[i] = auxarr[i];
        }
        number[len-1] = r;
    }else{
        number = auxarr;
    }
    auxarr = NULL;
    (*h).length = len;
    (*h).digits = number;
    return h;
}

HugeInteger *hugeDestroyer(HugeInteger *h){
    free((*h).digits);
    (*h).digits = NULL;
    free(h);
    return NULL;
}

char *fib_cache_botup(int a,HugeInteger *cache[]){
    HugeInteger *aux;
    char *str;
    int n;
    if(a<=2){
        return "1";
    }else{
        for (int i = 2; i < a; ++i){
            cache[2] = hugeAdd(cache[1], cache[0]);
            aux = cache[0];
            cache[0] = cache[1];
            cache[1] = cache[2];
            aux = hugeDestroyer(aux);
        }
    }
    n = (*cache[2]).length;
    str = (char *)malloc(sizeof(char)*(n+1));
    str[n] = '\0';
    for (int i = 0; i < n; ++i){
        str[i] = (char)(48 + (*cache[2]).digits[n-i-1]);
    }
    return str;
}