#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <arpa/inet.h>
#include <unistd.h>

static volatile int sigH = 0;

// Client Connections thread
void *ThreadFunc(void* data) {
    char  server_reply[2000];
    int recv_size;
    int *s = (int *)data;

    printf("Client Thread Created\n");

    while(1){
        //Receive a reply from the server
        if((recv_size = recv(*s , server_reply , 2000 , 0)) < 0){
            printf("recv failed %d\n", recv_size);
            break;
        }else{
        //Add a NULL terminating character to make it a proper string before printing
            server_reply[recv_size] = '\0';
            puts(server_reply);
        }

        if(sigH){
            break;
        }
    }
    return 0;
}

// Ctrl-C Handler
void consoleHandler(int signal) {
    printf("Ctrl-C handled\n");
    sigH = 1;
}

int main(int argc, char const *argv[]){
    // gcc client_linux.c -lpthread -o client_linux
    clock_t start = clock();
    pthread_t tid;
    // Client Vars
    int sockfd;
    struct sockaddr_in server;
    char message[100];
     
    // CLIENT SIDE ------------------------------------------------

    // Initialize a socket with his params
    if((sockfd = socket(AF_INET , SOCK_STREAM , 0 )) < 0){
        printf("Could not create socket.");
    }
 
    printf("Socket created.\n");

    // Try to conect to google server using sockaddr_in struct
    if(argc < 2){
        server.sin_addr.s_addr = inet_addr("192.168.1.101");
    }else{
        server.sin_addr.s_addr = inet_addr(argv[1]);
    }
    server.sin_family = AF_INET;
    server.sin_port = htons(8888);
 
    //Connect to remote server
    if (connect(sockfd , (struct sockaddr *)&server , sizeof(server)) < 0){
        puts("connect error");
        return 1;
    }

    signal(SIGINT, consoleHandler);

    pthread_create(&tid, NULL, ThreadFunc, &sockfd);

    puts("Connected");
    puts("Start sending messages:");
    while(1){
        fgets (message, 100, stdin);
        
        if(sigH){
            puts("Click any key to exit chat.");
            break;
        }
        //Send some data
        if(send(sockfd , message , strlen(message) , 0) < 0){
            puts("Send failed");
            return 1;
        }
        printf("Data Sent - ");

        
    }

    pthread_cancel(tid);
    // CleanUp and socket closing
    close(sockfd);
    pthread_join(tid, NULL);

    // Time running function
    clock_t end = clock();
    printf("Elapsed: %f seconds\n", (double)(end - start)/CLOCKS_PER_SEC);
    return 0;
}