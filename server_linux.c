#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <arpa/inet.h>
#include <unistd.h>

static volatile int sigH = 0;

// Struct for Server Messages Thread Handler
typedef struct ServerMessageThread{
    int **sockets;
    struct sockaddr_in *my_addr;
    int my_n;
}ServerMessageThread;

// Struct for Server Connections Thread Handler
typedef struct ServerConnectionThread{
    pthread_t *threads;
    int *total;
    int *socket;
}ServerConnectionThread;

// Ctrl-C Handler
void consoleHandler(int signal) {
    sigH = 1;
    printf("Ctrl-C handled\n");
}

// Server Messages Thread Handler
void *MessageThreadFunc(void* data) {
    ServerMessageThread *st = (ServerMessageThread *)data;
    int m, valread, MAXRECV = 1024;
    char *buffer, rcv_alert[50], n = 0;
    buffer = (char*) malloc((MAXRECV + 1) * sizeof(char));
    time_t t = time(NULL);
    struct tm *tm;

    printf("Server Thread #%d Created %s\n", (*st).my_n, inet_ntoa((*st->my_addr).sin_addr));
    while(1){
        valread = recv((*st->sockets[(*st).my_n]) , buffer, MAXRECV, 0);

        if(sigH){
            break;
        }
             
        if(valread < 0){
            //Somebody disconnected , get his details and print
            printf("Host disconnected unexpectedly , ip %s , port %d \n", inet_ntoa((*st->my_addr).sin_addr), 
                ntohs((*st->my_addr).sin_port));
            break;
        }

        if(valread == 0){
            //Somebody disconnected , get his details and print
            printf("Host disconnected , ip %s , port %d \n", inet_ntoa((*st->my_addr).sin_addr), ntohs((*st->my_addr).sin_port));
            break;
        }else{
            //add null character, if you want to use with printf/puts or other string handling functions
            buffer[valread] = '\0';
            if(! strncmp(buffer, "\\exit", 5)){
                break;
            }else{
                tm = localtime(&t);
                printf("%d/%d at %d:%d:%d de ", (*tm).tm_mon + 1, (*tm).tm_mday, (*tm).tm_hour, (*tm).tm_min, (*tm).tm_sec);
                //strftime (buffer,80,"Now it's %I:%M%p.",timeinfo);
                m = sprintf(rcv_alert, "recibido el %d/%d at %d:%d:%d", (*tm).tm_mon + 1, (*tm).tm_mday, 
                    (*tm).tm_hour, (*tm).tm_min, (*tm).tm_sec);
                send((*st->sockets[(*st).my_n]), rcv_alert, m, 0);
                printf("%s:%d: %s", inet_ntoa((*st->my_addr).sin_addr), ntohs((*st->my_addr).sin_port), buffer);
            }
        }
    }

    // Cleanup
    free(buffer);
    tm = NULL;
    buffer = NULL;
    printf("finished messages\n");
    return 0;
}

// Server Connections Thread Handler
void *ConnectionThreadFunc(void* data) {
    ServerConnectionThread *targ = (ServerConnectionThread *)data;
    int *sockets[10];
    ServerMessageThread *st[10];
    struct sockaddr_in *clients[10];
    char *hello, rcv_alert[50];
    int c = sizeof(struct sockaddr_in);
    hello = "Connected to Server.\n";

    while((*targ->total) < 10){
    // Accept incoming connections
        if((*targ->total) < 10){
            clients[(*targ->total)] = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
            sockets[(*targ->total)] = (int *)malloc(sizeof(int));

            if((*sockets[(*targ->total)] = accept((*targ->socket) ,(struct sockaddr *)clients[(*targ->total)], &c)) >= 0){
                puts("Connection accepted");
                send(*sockets[(*targ->total)], hello, strlen(hello) , 0);
                st[(*targ->total)] = (ServerMessageThread *)malloc(sizeof(ServerMessageThread));
                (*st[(*targ->total)]).sockets = sockets;
                (*st[(*targ->total)]).my_addr = clients[(*targ->total)];
                (*st[(*targ->total)]).my_n = (*targ->total);
                pthread_create(&(*targ).threads[(*targ->total)], NULL, MessageThreadFunc, st[(*targ->total)]);
                ++(*targ->total);
            }
        }

        if(sigH){
            break;
        }
    }

    for (int i = 0; i < (*targ->total); ++i){
        if(*sockets[i] < 0){
            printf("Socket %d failed.", i);
        }else{
            // Cleanup
            (*st[i]).my_addr = NULL;
            close(*sockets[i]);
            free(sockets[i]);
            sockets[i] = NULL;
            free(clients[i]);
            clients[i] = NULL;
            (*st[i]).sockets = NULL;
            free(st[i]);
        }
    }
    hello = NULL;
    printf("finished connections\n");
    return 0;
}
 
int main(int argc , char *argv[]){
    // gcc server_linux.c -lpthread -o server_linux
    int *s = (int *)malloc(sizeof(int));
    struct sockaddr_in server;
    int n = 0;
    pthread_t threads[11];
    ServerConnectionThread *targ = (ServerConnectionThread *)malloc(sizeof(ServerConnectionThread));

    //Replies to the client
     
    //Create a socket
    if((*s = socket(AF_INET, SOCK_STREAM, 0 )) < 0){
        printf("Could not create socket 0");
    }
    printf("Socket created.\n");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(8888);
     
    //Bind
    if(bind(*s ,(struct sockaddr *)&server, sizeof(server)) < 0){
        printf("Bind failed.");
    }
    puts("Bind done");
    
    //Listen
    listen(*s , 3);

    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    (*targ).total = &n;
    targ->threads = threads;
    (*targ).socket = s;

    signal(SIGINT, consoleHandler);

    pthread_create(&threads[0], NULL, ConnectionThreadFunc, targ);

    while(1){
        if(sigH){
            break;
        }
    }

    for (int i = 0; i < n; ++i){
        if(threads[i]){
            pthread_join(threads[i], NULL);
        }
    }

    // Waiting for connection handlers threads and closing his socket
    close(*s);
    free(s);
    s = NULL;
    pthread_join(threads[10], NULL);

    // Cleanup
    free(targ);
    printf("finished main\n");
    return 0;
}