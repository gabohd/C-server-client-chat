#include <stdio.h>
#include <time.h>

void main(int argc, char *argv[]){
    clock_t begin = clock();
    printf("Hello World!");
    clock_t end = clock();
    printf("Elapsed: %f seconds\n", (double)(end - begin) / CLOCKS_PER_SEC);
}